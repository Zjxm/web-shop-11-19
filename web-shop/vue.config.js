const path = require("path");
const ip = require("ip");
// const isProd = ["production"].includes(process.env.NODE_ENV);
require("babel-polyfill")

const TimeStamp = new Date().getTime();

function resolve(dir) {
    return path.join(__dirname, dir);
}

const pluginOptions = {
    publicPath: process.env.outputDir,
    host: ip.address(),
    port: 8088,
    cdnPath: "j.weizan.cn"
};

module.exports = {
    lintOnSave: false, // 是否开启eslint保存检测
    productionSourceMap: false,
    // productionSourceMap: !isProd,
    publicPath: pluginOptions.publicPath,
    assetsDir: 'admin-static',
    devServer: {
        port: pluginOptions.port,
        public: `${pluginOptions.host}:${pluginOptions.port}${pluginOptions.publicPath}`,
        disableHostCheck: true,
        proxy: {
            "/scrm-admin/": {
                target: "https://scrm-admin-test.vzan.com/",
                changeOrigin: true,
                ws: true
            },
            "/scrm-open-api/": {
                target: "https://scrm-test.vzan.com/",
                changeOrigin: true,
                ws: true
            }
        }
    },
    configureWebpack: {
        externals: {
            COS: "COS", // 腾讯云 https://github.com/tencentyun/cos-js-sdk-v5
            "clipboard.js": "ClipboardJS",
            'filterXSS': "filterXSS",
            'jquery': '$',
            'swiper': 'Swiper',
            'vue': 'Vue',
            'vue-router': 'VueRouter',
            'element-ui': 'ELEMENT',
            vuex: "Vuex",
            axios: "axios",
            wangeditor: "wangEditor",
        },
        output: {
            filename: `js/[name].${TimeStamp}.js`,
            chunkFilename: `js/[name].${TimeStamp}.js`
        }
    },
    chainWebpack: (config) => {
        config.resolve.alias
            .set("@", resolve("./src"))
            .set("@baseurl", resolve("./src/utils/baseUrl.js"))
            .set("@comp", resolve("./src/components"))
    }
};