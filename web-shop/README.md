# 微赞用户用户后台（重构）

访问地址

- 测试地址：[http://wtest.vzan.com/admin?zbid=0#/](http://wtest.vzan.com/admin?zbid=0#/)
- 预更新地址：[http://tuser.vzan.com/admin?zbid=0#/](http://wtest.vzan.com/admin?zbid=0#/)
- 正式地址：[http://live.vzan.com/admin?zbid=0#/](http://wtest.vzan.com/admin?zbid=0#/)


## 项目说明

基于 Webpack + Vue.js + elementUI + JavaScript(es6)。

以下是项目运行的详细说明，**如果还有其他问题，请询问负责项目相关同事，我们会尽力解答**。

## 页面总体规划

- [x] 前端自动化环境 webpack
- [x] 框架使用 Vue.js
- [x] 类库使用 JavaScript(es6)
- [x] UI 框架使用 ant-design-vue (elementUI 保持兼容旧功能代码, 不做新功能开发)

## 分支 (分支名称命名同步域名地址)

| 分支 | 名称 | 环境 | 备注 |
| --- | --- | --- | --- |
| master | 主分支 | 生产环境（受保护分支） | 永远是可用的、稳定的、可直接发布的版本，不能直接在该分支上开发 |
| tuser | 预更新分支 | 验收测试环境（数据同步生成环境） | 上正式环境前最后一步验收，修改好bug并确定稳定之后合并到master分支，然后发布master分支 |
| wtest | 开发主分支 | 功能验收测试环境 | 代码永远是最新，所有新功能以这个分支来创建自己的开发分支，该分支只做只合并操作，不能直接在该分支上开发 |

## 项目运行

```js
cd userlive

yarn install
```
> 项目运行  接口代理环境

```
yarn serve        // 测试环境

yarn serve --live   // 生产

yarn serve --tuser  // 预发布
```

```js
// 测试环境打包
yarn wtest
// 预更新环境打包
yarn tuser
```

```js
// 正式环境打包

yarn build
静态资源存放目录
https://j.weizan.cn/LiveAdmin/admin-static

index.html
https://live.vzan.com/admin

// webpack-bundle-analyzer
yarn build --report

yarn lint

// 更新依赖
npm-check-updates => 检查package.json版本 => ncu -u

```
